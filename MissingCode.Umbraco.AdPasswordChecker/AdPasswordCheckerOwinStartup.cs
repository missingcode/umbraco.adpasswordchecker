﻿using Owin;
using Umbraco.Core;
using Umbraco.Core.Models.Identity;
using Umbraco.Core.Security;
using Umbraco.Web.Security.Identity;

namespace MissingCode.Umbraco.AdPasswordChecker
{
    public class AdPasswordCheckerOwinStartup
    {
        public void Configuration(IAppBuilder app)
        {
            var applicationContext = ApplicationContext.Current;

            app.ConfigureUserManagerForUmbracoBackOffice<BackOfficeUserManager, BackOfficeIdentityUser>(
                ApplicationContext.Current,
                (options, context) =>
                {
                    var membershipProvider = MembershipProviderExtensions.GetUsersMembershipProvider().AsUmbracoMembershipProvider();

                    var store = new BackOfficeUserStore(
                                applicationContext.Services.UserService,
                                applicationContext.Services.EntityService,
                                applicationContext.Services.ExternalLoginService,
                                membershipProvider);

                    return AdPasswordUserManager.InitUserManager(new AdPasswordUserManager(store), membershipProvider, options);
                });

            //app.UseUmbracoBackOfficeCookieAuthentication(ApplicationContext.Current)
            //    .UseUmbracoBackOfficeExternalCookieAuthentication(ApplicationContext.Current);

            app.UseUmbracoBackOfficeCookieAuthentication(ApplicationContext.Current)
                .UseUmbracoBackOfficeExternalCookieAuthentication(ApplicationContext.Current)
                .UseUmbracoPreviewAuthentication(ApplicationContext.Current);
        }
    }
}
